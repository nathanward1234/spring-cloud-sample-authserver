This project is a simple, minimal implementation of an OAuth2
Authorization Server for use with Spring Cloud sample apps. It has a
context root of `/uaa` (so that it won't share cookies with other apps
running on other ports on the root resource). OAuth2 endpoints are:

* `/uaa/oauth/token` the Token endpoint, for clients to acquire access
  tokens. There is one client ("acme" with secret "acmesecret"). With
  Spring Cloud Security this is the `oauth2.client.tokenUri`.
* `/uaa/oauth/authorize` the Authorization endpoint to obtain user
  approval for a token grant.  Spring Cloud Security configures this
  in a client app as `oauth2.client.authorizationUri`.
* `/uaa/oauth/check_token` the Check Token endpoint (not part of the
  OAuth2 spec). Can be used to decode a token remotely. Spring Cloud
  Security configures this in a client app as
  `oauth2.resource.tokenInfoUri`.

# OAuth2

See also: 

* OAuth 2 in Action

## OAuth2 Key Concepts

There are several key concepts and terminology that are necessary to understand in order to talk about OAuth2. 

*resource owner* - The person (i.e. end user) who has access to a protected resource. 

*protected resource* - A system component that a resource owner has access to, such as a web service end point. 

*client* - the piece of software that access a protected resource on behalf of the resource owner. 

A *client* is the software application that needs to access a *protected resource* on behalf of a *resource owner*. 
For example, the web application that needs to call a web service with a token that indicates that the user has been authenticated, where
the web service is the *protected resource* and the end user is the *resource owner*.

In addition, OAuth2 defines several different scenarios or patterns of authentication to handle different types of situations. 
These types of flows are referred to as *grant types*.

An *Authorization Server* is trusted by the protected resource to issue special-purpose security credentials called OAuth access tokens. 

The token represents the access that’s been delegated to the client.

This tutorial is largely based on Chapter 7 of the book Microservices in Action. 

Use the `@EnableResourceServer` annotation on a Spring Boot application to indicate that the web service end points should be protected and require an OAuth2 token.

> It�s extremely important to understand that your JWT tokens are signed, but not encrypted. Any online JWT tool can decode         the JWT token and expose its contents. I bring this up because the JWT specification does allow you extend the token and add         additional information to the token. Don�t expose sensitive or Personally Identifiable Information (PII) in your JWT tokens.

Spring Microservices in Action, Chapter 7, section 7.4.1

#### Use a custom RestTemplate to propagate a JWT token in REST API calls.

To propogate an OAuth2 token from one service to another, the OAuth2RestTemlpate is typically used, but this does not work for JWT tokens. For JWT tokens to be propogated from one service to another, a custom RestTemplate been needs to be configured that will perform the injection of the JWT token into the request header for you. 

In order to inject the JWT token into the request header, the custom RestTemplate
uses an Interceptor named UserContextInterceptor. This Interceptor may eventually
be used to insert other information into the header such as a Correlation ID for app to app 
log tracing and other header information.

```
package com.demo;

import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.client.RestTemplate;

//Tell Spring Cloud Security that the web service end points 
//should be protected and require an OAuth2 token.
@EnableResourceServer
@SpringBootApplication
public class SpringCloudSecurityService1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudSecurityService1Application.class, args);
	}
	
	/**
	 * A custom RestTemplate that injects the JWT token into the request header.
	 * 
	 * This is necessary in order to propagate a JWT token from one service to another. 
	 * The @OAuth2RestTemplate annotation provided by Spring Cloud Security can be used for an OAuth2 token
	 * but not for a JWT token. 
	 */
	// The @Primary annotation indicates that a bean should be given preference when 
	// multiple candidates are qualified to autowire a single-valued dependency. 
	// It is used here to ensure that this class is used when an RestTemplate is Autowired.
    @Primary
    @Bean
    public RestTemplate customRestTemplate() {
        RestTemplate template = new RestTemplate();
        List<ClientHttpRequestInterceptor> interceptors = template.getInterceptors();
        
        // Add a UserContextInterceptor to the templates list of Interceptors
        if (interceptors == null) {
            template.setInterceptors(Collections.singletonList(new UserContextInterceptor()));
        } else {
            interceptors.add(new UserContextInterceptor());
            template.setInterceptors(interceptors);
        }
        
        return template;
    }
}
```

```
package com.demo;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * A ClientHttpRequestInterceptor that is used by a custom RestTemplate to
 * intercept REST API calls and add the OAuth2 token to the request header.
 * 
 * The ClientHttpRequestInterceptor is a Spring class that allows you to hook in
 * functionality to be executed before a REST-based call is made.
 * 
 * This Interceptor may eventually be used to insert other information 
 * into the header such as a Correlation ID for app to app 
 * log tracing and other header information.
 */
public class UserContextInterceptor implements ClientHttpRequestInterceptor {

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		HttpHeaders headers = request.getHeaders();
		headers.add(UserContext.AUTH_TOKEN, UserContextHolder.getContext().getAuthToken());

		return execution.execute(request, body);
	}
}
```


#### Use a custom servlet filter (named UserContextFilter) to parse out the authentication token from the HTTP header. 

```
package com.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * A custom servlet filter that parses out the OAuth2 token from the
 * HTTP header for any web page or RESTful web service related HTTP requests
 * and store the token in the UserContextHolder for use when invoking other
 * REST APIs via a RestTemplate.
 */
@Component
public class UserContextFilter implements Filter {
	private static final Logger logger = LoggerFactory.getLogger(UserContextFilter.class);

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

		logger.debug("Entiering service1 service with auth token: ", httpServletRequest.getHeader("Authorization"));

		UserContextHolder.getContext().setAuthToken(httpServletRequest.getHeader(UserContext.AUTH_TOKEN));

		filterChain.doFilter(httpServletRequest, servletResponse);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
}
```


```
package com.demo;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * A ClientHttpRequestInterceptor that is used by a custom RestTemplate to
 * intercept REST API calls and add the OAuth2 token to the request header.
 * 
 * The ClientHttpRequestInterceptor is a Spring class that allows you to hook in
 * functionality to be executed before a REST-based call is made.
 */
public class UserContextInterceptor implements ClientHttpRequestInterceptor {

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		HttpHeaders headers = request.getHeaders();
		headers.add(UserContext.AUTH_TOKEN, UserContextHolder.getContext().getAuthToken());

		return execution.execute(request, body);
	}
}
```
